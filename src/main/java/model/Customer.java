/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author BenzNarathip
 */
public class Customer {
    private int Customer_ID;
    private String FirstName;
    private String LastName;
    private String Tel;

    public Customer(int Customer_ID, String FirstName, String LastName, String Tel) {
        this.Customer_ID = Customer_ID;
        this.FirstName = FirstName;
        this.LastName = LastName;
        this.Tel = Tel;
    }

    public Customer() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int getCustomer_ID() {
        return Customer_ID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public String getTel() {
        return Tel;
    }

    public void setCustomer_ID(int Customer_ID) {
        this.Customer_ID = Customer_ID;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public void setTel(String Tel) {
        this.Tel = Tel;
    }

    @Override
    public String toString() {
        return "Customer{" + "Customer_ID=" + Customer_ID + ", FirstName=" + FirstName + ", LastName=" + LastName + ", Tel=" + Tel + '}';
    }


}
