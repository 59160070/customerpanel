/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.mycompany.customer.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;

/**
 *
 * @author BenzNarathip
 */
public class CustomerDao implements DaoInterface<Customer> {

    @Override
    public int add(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Customer (FirstName, LastName,Tel) VALUES (?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            int row =stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();            
            if(result.next()) {
                id = result.getInt(1);
            }            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Customer> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Customer_ID, FirstName, LastName, Tel FROM Customer";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while(result.next()) {
                int Customer_ID = result.getInt("Customer_ID");
                String FirstName = result.getString("FirstName");
                String LastName = result.getString("LastName");
                String Tel = result.getString("Tel");
                Customer customer = new Customer(Customer_ID, FirstName, LastName,Tel);
                list.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Customer get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT Customer_ID, FirstName, LastName, Tel FROM Customer WHERE Customer_ID="+id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if(result.next()) {
                int cid = result.getInt("Customer_ID");
                String FirstName = result.getString("FirstName");
                String LastName = result.getString("LastName");
                String Tel = result.getString("Tel");
                Customer customer = new Customer(cid, FirstName, LastName,Tel);
                return customer;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM Customer WHERE Customer_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row =stmt.executeUpdate();            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Customer object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE Customer SET FirstName = ?, LastName = ?,Tel = ? WHERE Customer_ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getFirstName());
            stmt.setString(2, object.getLastName());
            stmt.setString(3, object.getTel());
            stmt.setInt(4, object.getCustomer_ID());
            row =stmt.executeUpdate();            
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }
    
    public static void main(String[]args){
        CustomerDao dao = new CustomerDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
                
        int id = dao.add(new Customer(-1,"tests","Tesst","000000001"));
        System.out.println("id:"+ id);
        System.out.println(dao.get(id));       
        
        Customer lastCustomer = dao.get(id);        
        System.out.println("lastCustomer "+lastCustomer);
        lastCustomer.setTel("1000");
        int row = dao.update(lastCustomer);
        Customer updateCustomer = dao.get(id);
        System.out.println("updateCustomer "+updateCustomer);
        
        dao.delete(id);
        Customer deleteCustomer = dao.get(id);
        System.out.println("delete Customer "+deleteCustomer); 
    }    
}
